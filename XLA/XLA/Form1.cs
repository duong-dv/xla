﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XLA
{
    public partial class Form1 : Form
    {
        Bitmap bitmap, bitmap1,bitmapSave;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                bitmap = new Bitmap(open.FileName);
                bitmap1 = bitmap;
                pictureBox1.Image = bitmap;
            }
        }

        unsafe
        public BitmapData gray_image(BitmapData data)
        {
            int offset = data.Stride - bitmap.Width * 3;

            byte* p = (byte*)data.Scan0;

            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    int t = (p[0] + p[1] + p[2]) / 3;
                    p[0] = (byte)t;
                    p[1] = (byte)t;
                    p[2] = (byte)t;

                    p += 3;
                }
                p += offset;
            }
            return data;
        }

        public static Bitmap Dilate(Bitmap Image, int Size)
        {
            System.Drawing.Bitmap TempBitmap = Image;
            System.Drawing.Bitmap NewBitmap = new System.Drawing.Bitmap(TempBitmap.Width, TempBitmap.Height);
            System.Drawing.Graphics NewGraphics = System.Drawing.Graphics.FromImage(NewBitmap);
            NewGraphics.DrawImage(TempBitmap, new System.Drawing.Rectangle(0, 0, TempBitmap.Width, TempBitmap.Height), new System.Drawing.Rectangle(0, 0, TempBitmap.Width, TempBitmap.Height), System.Drawing.GraphicsUnit.Pixel);
            NewGraphics.Dispose();
            Random TempRandom = new Random();
            int ApetureMin = -(Size / 2);
            int ApetureMax = (Size / 2);
            for (int x = 0; x < NewBitmap.Width; ++x)
            {
                for (int y = 0; y < NewBitmap.Height; ++y)
                {
                    int RValue = 0;
                    int GValue = 0;
                    int BValue = 0;
                    for (int x2 = ApetureMin; x2 < ApetureMax; ++x2)
                    {
                        int TempX = x + x2;
                        if (TempX >= 0 && TempX < NewBitmap.Width)
                        {
                            for (int y2 = ApetureMin; y2 < ApetureMax; ++y2)
                            {
                                int TempY = y + y2;
                                if (TempY >= 0 && TempY < NewBitmap.Height)
                                {
                                    Color TempColor = TempBitmap.GetPixel(TempX, TempY);
                                    if (TempColor.R > RValue)
                                        RValue = TempColor.R;
                                    if (TempColor.G > GValue)
                                        GValue = TempColor.G;
                                    if (TempColor.B > BValue)
                                        BValue = TempColor.B;
                                }
                            }
                        }
                    }
                    Color TempPixel = Color.FromArgb(RValue, GValue, BValue);
                    NewBitmap.SetPixel(x, y, TempPixel);
                }
            }
            return NewBitmap;
        }


        public static Bitmap DilateAndErodeFilter(Bitmap sourceBitmap,
                                               int matrixSize)
        {
            BitmapData sourceData =
                       sourceBitmap.LockBits(new Rectangle(0, 0,
                       sourceBitmap.Width, sourceBitmap.Height),
                       ImageLockMode.ReadOnly,
                       PixelFormat.Format32bppArgb);

            byte[] pixelBuffer = new byte[sourceData.Stride *
                                          sourceData.Height];

            byte[] resultBuffer = new byte[sourceData.Stride *
                                           sourceData.Height];

            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0,
                                       pixelBuffer.Length);

            sourceBitmap.UnlockBits(sourceData);

            int filterOffset = matrixSize;
            int calcOffset = 0;

            int byteOffset = 0;

            byte blue = 0;
            byte green = 0;
            byte red = 0;

            byte morphResetValue = 0;


            morphResetValue = 255;


            for (int offsetY = filterOffset; offsetY <
                sourceBitmap.Height - filterOffset; offsetY++)
            {
                for (int offsetX = filterOffset; offsetX <
                    sourceBitmap.Width - filterOffset; offsetX++)
                {
                    byteOffset = offsetY *
                                 sourceData.Stride +
                                 offsetX * 4;

                    blue = morphResetValue;
                    green = morphResetValue;
                    red = morphResetValue;


                    for (int filterY = -filterOffset;
                        filterY <= filterOffset; filterY++)
                    {
                        for (int filterX = -filterOffset;
                            filterX <= filterOffset; filterX++)
                        {

                            calcOffset = byteOffset +
                                         (filterX * 4) +
                            (filterY * sourceData.Stride);

                            if (pixelBuffer[calcOffset] < blue)
                            {
                                blue = pixelBuffer[calcOffset];
                            }

                            if (pixelBuffer[calcOffset + 1] < green)
                            {
                                green = pixelBuffer[calcOffset + 1];
                            }

                            if (pixelBuffer[calcOffset + 2] < red)
                            {
                                red = pixelBuffer[calcOffset + 2];
                            }
                        }
                    }



                    resultBuffer[byteOffset] = blue;
                    resultBuffer[byteOffset + 1] = green;
                    resultBuffer[byteOffset + 2] = red;
                    resultBuffer[byteOffset + 3] = 255;
                }
            }

            Bitmap resultBitmap = new Bitmap(sourceBitmap.Width,
                                             sourceBitmap.Height);

            BitmapData resultData =
                       resultBitmap.LockBits(new Rectangle(0, 0,
                       resultBitmap.Width, resultBitmap.Height),
                       ImageLockMode.WriteOnly,
                       PixelFormat.Format32bppArgb);

            Marshal.Copy(resultBuffer, 0, resultData.Scan0,
                                       resultBuffer.Length);

            resultBitmap.UnlockBits(resultData);

            return resultBitmap;
        }



        public Bitmap Binary_image(Bitmap data)
        {
            for (int i = 0; i < data.Width; i++)
            {
                for (int j = 0; j < data.Height; j++)
                {
                    Color ori = data.GetPixel(i, j);
                    if (ori.R < 128)
                    {
                        data.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                    }
                    else
                    {
                        data.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                    }
                }
            }
            return data;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            BitmapData data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
            bitmap.UnlockBits(gray_image(data));
            pictureBox2.Image = bitmap;
            bitmapSave = bitmap;
            bitmap = bitmap1;


        }

        private void binary_Click(object sender, EventArgs e)
        {
            bitmap = Binary_image(bitmap);
            pictureBox2.Image = bitmap;
            bitmapSave = bitmap;
            bitmap = bitmap1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtsize.Text.ToString() != "")
            {
                int t = Int32.Parse(txtsize.Text.ToString());
                bitmap = Dilate(bitmap, t);
                pictureBox2.Image = bitmap;
                bitmapSave = bitmap;
                bitmap = bitmap1;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txtsize.Text.ToString() != "")
            {
                int t = Int32.Parse(txtsize.Text.ToString());
                bitmap = DilateAndErodeFilter(bitmap, t);
                pictureBox2.Image = bitmap;
                bitmapSave = bitmap;
                bitmap = bitmap1;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SaveFileDialog f = new SaveFileDialog();
            f.Filter = "JPG(*.JPG)|*.jpg";
            if (f.ShowDialog() == DialogResult.OK)
            {
                bitmapSave.Save(f.FileName);
            }
        }
    }
}
